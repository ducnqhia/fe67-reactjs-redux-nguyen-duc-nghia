import React, { Component } from "react";
import Card from "./Card";
import Detail from "./Detail";
import ProductList from "./ProductList";
import { connect } from "react-redux";

class Home extends Component {
  cartQuatity = () => {
    const result = this.props.card.reduce((a, b) => (a = a + b.quantity), 0);
    return result;
  };
  render() {
    return (
      <div className="container">
        <h1 className="text-success text-center mt-3">Bài tập giỏ hàng</h1>
        <h4
          className="text-danger text-right"
          data-toggle="modal"
          data-target="#gioHangModal"
        >
          Giỏ hàng ({this.cartQuatity()})
        </h4>
        <Card />
        <ProductList />
        <Detail />
      </div>
    );
  }
}
const mapStateToProp = (state) => {
  return {
    card: state.product.card,
  };
};

export default connect(mapStateToProp)(Home);
