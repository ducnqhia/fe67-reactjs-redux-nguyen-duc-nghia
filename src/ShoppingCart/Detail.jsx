import React, { Component } from "react";
import { connect } from "react-redux";
class Detail extends Component {
  render() {
    const { name, img, screen, backCamera, frontCamera, desc } =
      this.props.selectedProduct;

    return (
      <div className="row my-3">
        <div className="col-5">
          <h5>{name ? name : "Tên sản phẩm"}</h5>
          <img
            className="w-75"
            src={img ? img : this.props.product.img}
            alt={`Hình ảnh ${name ? name : this.props.product.name}`}
          ></img>
        </div>
        <div className="col-7">
          <h5>Thông tin chi tiết</h5>
          <table className="table">
            <tbody>
              <tr>
                <td className="w-25">Màn hình</td>
                <td className="w-75">
                  {screen ? screen : this.props.product.screen}
                </td>
              </tr>
              <tr>
                <td className="w-25">Camera trước</td>
                <td className="w-75">
                  {frontCamera ? frontCamera : this.props.product.frontCamera}
                </td>
              </tr>
              <tr>
                <td className="w-25">Camera sau</td>
                <td className="w-75">
                  {backCamera ? backCamera : this.props.product.backCamera}
                </td>
              </tr>
              <tr>
                <td className="w-25">Mô tả</td>
                <td className="w-75">
                  {desc ? desc : this.props.product.desc}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    selectedProduct: state.product.selectedProduct,
    product: state.product.products[0],
  };
};
export default connect(mapStateToProps)(Detail);
